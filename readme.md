# Less1.4 by @michailko

Container runs on 8009 external port

Command to run container: \
<code>sudo docker run -p 8009:80 -d  -v $(pwd)/conf:/usr/local/nginx/conf <image\:tag></code>

Resulted image size: ~ 111 MB (369 on build stage)