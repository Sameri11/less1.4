FROM debian:9 AS build

# Download and unpack neccesary libs and plugins
RUN apt update \
    && apt install -y wget gcc make g++ \
    && wget https://ftp.pcre.org/pub/pcre/pcre-8.36.tar.gz \
    && wget https://www.zlib.net/zlib-1.2.11.tar.gz \
    && wget https://github.com/openresty/luajit2/archive/v2.1-20200102.tar.gz \
    && wget https://github.com/vision5/ngx_devel_kit/archive/v0.3.1.tar.gz \
    && wget https://github.com/openresty/lua-nginx-module/archive/v0.10.16rc5.tar.gz \
    && wget http://nginx.org/download/nginx-1.17.10.tar.gz \
    && wget https://github.com/openresty/lua-resty-core/archive/v0.1.18rc4.tar.gz \
    && wget https://github.com/openresty/lua-resty-lrucache/archive/v0.10rc1.tar.gz \
    && ls | grep 'tar.gz' | xargs -t -n1 tar xf \
    && ls | grep 'tar.gz' | xargs -t rm -rf

# Install luaJIT and restyCore libraries for nginx build
RUN cd $(ls | grep 'luajit') \
    && make \
    && make install \
    && cd /

RUN cd $(ls | grep 'lua-resty-core') \
    && make \
    && make install \
    && cd /

RUN cd $(ls | grep 'lua-resty-lrucache') \
    && make \
    && make install \
    && cd /

# Install nginx with modules
RUN export LUAJIT_LIB=/usr/local/lib \
    && export LUAJIT_INC=/usr/local/include/luajit-2.1 \
    && cd $(ls | grep 'nginx-1') \
    && ./configure \
       --prefix=/usr/local/nginx/ \
       --with-ld-opt="-Wl,-E,-rpath,${LUAJIT_LIB}" \
       --with-pcre-jit \
       --with-pcre=/$(ls / | grep 'pcre') \
       --with-zlib=/$(ls / | grep 'zlib') \
       --add-module=/$(ls / | grep 'ngx_devel_kit') \
       --add-module=/$(ls / | grep 'lua-nginx-module') \
    && make \
    && make install

FROM debian:9
COPY --from=build /usr/local/ /usr/local/
RUN mkdir -p /usr/local/share/lua/5.1/resty/lrucache \
    && ln -s /usr/local/lib/lua/resty/core.lua /usr/local/share/lua/5.1/resty/core.lua \
    && ln -s /usr/local/lib/lua/resty/core /usr/local/share/lua/5.1/resty/core \
    && ln -s /usr/local/lib/lua/resty/lrucache.lua /usr/local/share/lua/5.1/resty/lrucache.lua \
    && ln -s /usr/local/lib/lua/resty/lrucache /usr/local/share/lua/5.1/resty/lrucache
WORKDIR /usr/local/nginx/sbin/
CMD ["./nginx", "-g", "daemon off;"]


